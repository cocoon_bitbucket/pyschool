
# Overview

this repository show some practice around python test and code organisation

always create a git respository for your project
always install pip (if not present)
always create a virtualenv 
always create a README.md file 


note the following files

## .gitignore
the files we want to exclude from our repository ( git commit)

## requirements.txt
the repositories we need for this project
    pip install -r requirements.txt


## setup.py 
the installation file of your project 
    git clone https://cocoon_bitbucket@bitbucket.org/cocoon_bitbucket/pyschool.git
    cd pyschool
    python setup.py install




# the project

## utils/algebra
a library definig basic algebra functions 

1) run it 

    cd utils
    python algebra.py
    

2) change the DEBUG level to ERROR 

and run it again


## draft

draft is a python program using algebra library

run it
obsserve the difference in the logs



## tests directory

run the tests ( pytest)

activate the virtualenv
ensure your project is in python path

at the project root

    /$HOME/v-pyschool/bin/activate
    export PYTHONPATH="./"
    pytest

all the files with  test_*.py and *_test.py will run



## fixtures

see code utils/wallet.py




# biblio

https://docs.pytest.org/en/latest/index.html

https://semaphoreci.com/community/tutorials/testing-python-applications-with-pytest
