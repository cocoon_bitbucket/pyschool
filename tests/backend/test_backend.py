


def test_basic(backend):
    """


    :param empty_backend:
    :return:
    """
    backend.show()

    assert backend.db.get("version")=="0.1"
    assert backend.db.exists("created")
    assert len(backend.db.keys()) == 2

    return


def test_core_backend(empty_backend):
    """


    :param empty_backend:
    :return:
    """
    assert len(empty_backend.db.keys()) == 0
    return