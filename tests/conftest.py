import pytest
import os
from backend import Backend


#backend_filename= "samples/backend.db"



def samples_directory():

    here = os.path.dirname(os.path.realpath(__file__))
    #backend_filename= os.path.join( here ,"tests/samples/backend.db")

    return here

def backend_filename():
    backend_filename= os.path.join( samples_directory() ,"samples/backend.db")
    return backend_filename




@pytest.fixture
def empty_backend():
    '''Returns a fresh ne backend'''
    b = Backend(backend_filename())
    b.clear()
    return b

@pytest.fixture
def backend():
    '''Returns a initialized backend'''
    b = Backend(backend_filename())
    b.clear()
    b.setup()
    return b


