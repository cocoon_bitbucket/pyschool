from db import mydb
import datetime
import logging
log= logging.getLogger("__name__")



default_filename= "backend.db"



class Backend(object):
    """



    """
    def __init__(self,filename=None):
        """

        :param db:
        """
        self.filename = filename or default_filename
        self.db = mydb.MyDb(self.filename)


    def setup(self):
        """

        :return:
        """
        self.db.set("version", "0.1")
        self.db.set("created",datetime.datetime.now())


    def show(self):
        """

        :return:
        """
        print("version: %s" % self.db.get("version"))
        print("created: %s" % self.db.get("created"))



    def clear(self):
        """

        :return:
        """
        self.db.clear()
        self.db=  mydb.MyDb(self.filename)

if __name__=="__main__":

    logging.basicConfig(level=logging.DEBUG)


    filename = "../tests/samples/mydb.db"


    log.info("enter backend")


    b = Backend(filename)
    b.clear()
    b.setup()

    b.show()


    log.info("leave backend")