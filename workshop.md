

# prerequisites

* python 2.7
* pip
* virtualenv

* pycharm Community edition

# Install

    git clone https://cocoon_bitbucket@bitbucket.org/cocoon_bitbucket/pyschool.git
    cd pyschool
    
    virtualenv v-pyschool
    source v-pyschool/bin/activate
    
    pip install -r requirements.txt
    export PYTHONPATH="."
    
    pytest
    
    
    