import os
import shelve

import logging
log = logging.getLogger(__name__)



class MyDb(object):
    """


    """
    def __init__(self,filename):
        """

        :param filename:
        """
        # load or create the database
        self.db = shelve.open(filename)
        self.filename= filename

    def get(self,key):
        """

        :param key:
        :return:
        """
        return self.db[key]

    def set(self, key,value):
        """

        :param key:
        :return:
        """
        self.db[key]= value
        #self.db.sync()

    def delete(self,key):
        """

        :param key:
        :return:
        """
        del self.db[key]

    def exists(self,key):
        """

        :param key:
        :return:
        """
        try:
            v = self.db[key]
            return True
        except Exception as e:
            return False

    def keys(self):
        """

        :return:
        """
        return self.db.keys()

    def close(self):
        """

        :return:
        """
        self.db.close()

    def clear(self):
        """

        :return:
        """
        self.db.close()
        os.unlink(self.filename)

if __name__=="__main__":

    logging.basicConfig(level=logging.DEBUG)

    filename= "../tests/samples/mydb.db"

    log.info("Enter mydb")

    # database = shelve.open(filename)
    # try:
    #     object = database["key"]
    #     assert object == "value"
    # except Exception as e:
    #     print(e)
    #
    # database['key'] = "value"


    db = MyDb(filename)


    if not db.exists("key"):
        db.set("key","value")

    assert db.exists("key") == True

    v = db.get("key")
    assert v == "value"

    db.delete("key")

    assert db.exists("key") == False

    db.clear()

    try:
        os.stat(filename)
    except Exception as e:
        assert e.errno == 2

    log.info("Leave mydb")


