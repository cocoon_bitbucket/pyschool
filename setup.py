from distutils.core import setup

setup(
    name='pyschool',
    version='0.1',
    packages=['utils','backend',"db"],
    url='https://cocoon_bitbucket@bitbucket.org/cocoon_bitbucket/pyschool.git',
    license='MIT',
    author='cocoon',
    author_email='laurent.tordjman@orange.com',
    description='a sample python project'
)
