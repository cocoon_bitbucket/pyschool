import logging

log = logging.getLogger(__name__)


def multiply(x, y):
    """

    :param x:
    :param y:
    :return:
    """
    log.debug("multiply(%d,%d)" % (x, y))
    return x * y


def add(x, y):
    """

    :param x:
    :param y:
    :return:
    """
    log.debug("add(%d,%d)" % (x, y))
    return x + y


def substract(x, y):
    """

    :param x:
    :param y:
    :return:
    """
    log.debug("substrract(%d,%d)" % (x, y))
    return x - y


def divide(x, y):
    """

    :param x:
    :param y:
    :return:
    """
    log.debug("divide(%d,%d)" % (x, y))
    return x / y


if __name__ == ("__main__"):
    """
    """
    logging.basicConfig(level=logging.DEBUG)

    log.info("start of algebra")

    z = multiply(10, 5)
    assert z == 50

    z = add(10, 5)
    assert z == 15

    z = divide(10, 5)
    assert z == 2

    z = substract(10, 5)
    assert z == 5

    log.info("end of algebra")

    """





    """